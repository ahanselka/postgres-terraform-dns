CREATE TABLE domains (
    id SERIAL NOT NULL PRIMARY KEY,
    domain_name TEXT UNIQUE NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL
);

CREATE TABLE records (
    id SERIAL NOT NULL PRIMARY KEY,
    domain_id INT NOT NULL REFERENCES domains(id),
    name TEXT NOT NULL,
    type TEXT NOT NULL DEFAULT 'A',
    rrdata TEXT NOT NULL,
    ttl INT,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE,
    is_active BOOLEAN DEFAULT true NOT NULL
);

CREATE TABLE record_updates (
    record_id INT NOT NULL REFERENCES records(id),
    domain_id INT NOT NULL REFERENCES domains(id),
    record_name TEXT,
    rrdata TEXT,
    ttl INT,
    performed_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    is_active BOOLEAN NOT NULL,
    PRIMARY KEY (record_id, performed_at)
);


CREATE OR REPLACE FUNCTION trigger_on_record_update()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL AS $body$
BEGIN
    INSERT INTO record_updates (record_id, domain_id, record_name, rrdata, ttl, is_active)
    VALUES (old.id, old.domain_id, old.name, old.rrdata, old.ttl, old.is_active );
    RETURN new;
END; $body$;

CREATE OR REPLACE FUNCTION trigger_on_record_insert()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL AS $body$
BEGIN
    INSERT INTO record_updates (record_id, performed_at, domain_id, record_name, rrdata, ttl, is_active)
    VALUES (new.id, new.created_at, new.domain_id, new.name, new.rrdata, new.ttl, new.is_active );
    RETURN new;
END; $body$;


CREATE TRIGGER trigger_record_update
  BEFORE UPDATE
  ON records
  FOR EACH ROW
EXECUTE PROCEDURE trigger_on_record_update();

CREATE TRIGGER trigger_record_insert
  AFTER INSERT
  ON records
  FOR EACH ROW
EXECUTE PROCEDURE trigger_on_record_insert();

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER set_updated_timestamp
BEFORE UPDATE ON records
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
