This project uses the "last resort" [terraform external provider](https://registry.terraform.io/providers/hashicorp/external/latest/docs) to run a ruby script that gets DNS records from a PostgreSQL database.

I was surprised to discover that there aren't any postgresql data sources for Terraform, so I ended up writing this instead.
It is simple and very limited in use, but far easier for me than writing a new postgresql datasource (maybe someday `;)`). 

The schema includes triggers and functions to keep track of record changes as well. This is optional for the script to work.

The external provider only allows return of a JSON of strings, so the script returns a JSON key with one value... that is in fact JSON that is later read in by `jsondecode()`. 
It definitely feels really hacky, but it works!

The example terraform is using the [hetznerdns plugin](https://registry.terraform.io/providers/timohirt/hetznerdns) by [Timo Hirt](https://github.com/timohirt).