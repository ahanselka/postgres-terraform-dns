require 'pg'

inp = JSON.parse($stdin.read)

c = PG.connect( host: inp["pg_host"], user: inp["pg_user"], password: inp["pg_password"], dbname: inp["pg_database"] )
r = c.exec( "SELECT records.id, name, type, rrdata, ttl 
            FROM records 
            JOIN domains ON domain_id = domains.id
            WHERE is_active = true AND domains.domain_name = \'#{inp['domain']}\'" )

rstring = JSON.generate(r.entries)
rhash = { "json" => rstring }
puts JSON.generate(rhash)
