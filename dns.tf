locals {
  examplecom = jsondecode(data.external.example_com.result["json"])
}

data "external" "example_com" {
  program = ["ruby", "${path.module}/bin/pg_data.rb"]
  query = {
    pg_host     = var.pg_host
    domain      = "example.com"
    pg_user     = var.pg_user
    pg_database = var.pg_database
    pg_password = var.pg_password
  }
}

resource "hetznerdns_record" "example_com" {
  for_each = { for r in local.examplecom : r.id => r }
  zone_id  = hetznerdns_zone.example_com.id
  name     = each.value.name
  value    = each.value.rrdata
  type     = each.value.type
  ttl      = each.value.ttl
}